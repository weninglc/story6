from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    # path('url-yang-diinginkan/', views.nama_fungsi, name='nama_fungsi')
    path('', views.homepage, name='homepage'),
]
