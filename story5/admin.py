from django.contrib import admin
from .models import Subject, Assignment

# Register your models here.


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('matkul', 'dosen', 'sks', 'semester', 'kelas')
    ordering = ['semester']


admin.site.register(Subject, SubjectAdmin)


class AssignmentAdmin(admin.ModelAdmin):
    list_display = ('matkul', 'deskripsi', 'deadline')
    ordering = ('matkul', 'deadline')


admin.site.register(Assignment, AssignmentAdmin)
