from django.forms import ModelForm
from .models import Subject, Assignment


class SubjectForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Subject
        fields = '__all__'


class AssignmentForm(ModelForm):
    class Meta:
        ordering = ['deadline']
