from django.db import models

# Create your models here.


class Subject(models.Model):
    matkul = models.CharField('Subject', max_length=120, null=True)
    dosen = models.CharField('Lecturer', max_length=120, null=True)
    sks = models.IntegerField('Total Credit', null=True)
    semester = models.IntegerField('Semester', null=True)
    kelas = models.CharField('Class', max_length=120, null=True)

    def __str__(self):
        return self.matkul


class Assignment(models.Model):
    matkul = models.ForeignKey(Subject, on_delete=models.CASCADE, null=True)
    deskripsi = models.TextField('Description', blank=True)
    deadline = models.DateField('Deadline', null=True)
