from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.subject, name='subject'),
    path('add/', views.subject_add, name='subject_add'),
    path('detail/<int:index>/', views.subject_detail, name='subject_detail'),
    # path('list/', views.subject, ),
]
