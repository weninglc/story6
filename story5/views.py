from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Subject, Assignment
from .forms import SubjectForm
import datetime

# Create your views here.


def subject(request):
    if request.method == "POST":
        Subject.objects.get(id=request.POST['id']).delete()
        return redirect('/subject/')
    subject_list = Subject.objects.all()
    return render(request, 'subject_list.html', {'subject_list': subject_list})


def subject_add(request):
    submit = False
    if request.method == 'POST':
        form = SubjectForm(request.POST)
        if form.is_valid():
            form.save()
            # return render(request, 'subject_add.html')
            return HttpResponseRedirect('/subject/add/?submit=True')
    else:
        form = SubjectForm()
        if 'submit' in request.GET:
            submit = True
    return render(request, 'subject_add.html', {'form': form, 'submit': submit})


def subject_detail(request, index):
    subject = Subject.objects.get(pk=index)
    assignment = Assignment.objects.filter(matkul_id=subject)

    today = assignment.filter(deadline__lt=datetime.date.today())
    near = assignment.filter(deadline__range=(
        datetime.date.today(), datetime.date.today() + datetime.timedelta(days=6)))
    far = assignment.filter(
        deadline__gte=datetime.date.today() + datetime.timedelta(days=7))

    tasks = {
        'subject': subject,
        'today': today,
        'near': near,
        'far': far,
    }

    return render(request, 'subject_detail.html', tasks)
