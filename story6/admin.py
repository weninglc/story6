from django.contrib import admin
from .models import Activity, Participant

# Register your models here.


class ActivityAdmin(admin.ModelAdmin):
    list_display = ('kegiatan', 'deskripsi', 'tanggal')
    ordering = ['tanggal']


admin.site.register(Activity, ActivityAdmin)


class ParticipantAdmin(admin.ModelAdmin):
    list_display = ('kegiatan', 'nama')


admin.site.register(Participant, ParticipantAdmin)
