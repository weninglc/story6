from django.forms import ModelForm
from .models import Activity, Participant


class ActivityForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Activity
        fields = '__all__'


class ParticipantForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Participant
        fields = '__all__'
