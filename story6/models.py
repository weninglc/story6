from django.db import models

# Create your models here.


class Activity(models.Model):
    kegiatan = models.CharField('Activity', max_length=120, null=True)
    deskripsi = models.TextField('Description', blank=True)
    tanggal = models.DateField('Date', null=True)

    def __str__(self):
        return self.kegiatan


class Participant(models.Model):
    kegiatan = models.ForeignKey(Activity, on_delete=models.CASCADE, null=True)
    nama = models.CharField('Name', max_length=120, null=True)

    def __str__(self):
        return self.nama
