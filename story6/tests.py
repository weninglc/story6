from django.test import TestCase, Client
from django.urls import resolve
from .views import activity, activity_add
from .models import Activity, Participant
from .forms import ActivityForm, ParticipantForm


# Create your tests here.
class Story6UnitTest(TestCase):
    def test_activity_url_is_exist(self):
        response = Client().get('/activity')
        self.assertEqual(response.status_code, 301)

    def test_activity_using_func(self):
        found = resolve('/activity/')
        self.assertEqual(found.func, activity)

    def test_model_can_create_new_activity(self):
        activity_new = Activity.objects.create(
            kegiatan='Unit testing', deskripsi='Unit testing', tanggal='2020-03-05')
        self.assertTrue(isinstance(activity_new, Activity))
        self.assertEqual('Unit testing', str(activity_new))
        self.assertTrue(Client().get('/?submit=True'))

    def test_form_activity_validation_for_blank_items(self):
        form = ActivityForm(
            data={'kegiatan': '', 'deskripsi': '', 'tanggal': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['kegiatan'], ["This field is required."])
        self.assertEqual(form.errors['tanggal'], ["This field is required."])

    def test_form_activity_validation_accepted(self):
        form = ActivityForm(
            data={'kegiatan': 'Unit testing', 'deskripsi': 'Unit testing', 'tanggal': '2020-03-05'})
        self.assertTrue(form.is_valid())

    def test_activity_add_url_is_exist(self):
        response = Client().get('/activity/add/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_add_new_participant(self):
        participant_new = Participant.objects.create(nama='Unit testing')
        self.assertTrue(isinstance(participant_new, Participant))
        self.assertEqual('Unit testing', str(participant_new))

    def test_form_participant_validation_for_blank_items(self):
        form = ParticipantForm(data={'nama': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['nama'], ["This field is required."])

    def test_form_participant_validation_accepted(self):
        activity_new = Activity.objects.create(
            kegiatan='Unit testing', deskripsi=' Unit testing', tanggal='2020-03-05')
        form = ParticipantForm(data={'kegiatan': activity_new, 'nama': 'sdql'})
        self.assertTrue(form.is_valid())

    def test_activity_add_using_func(self):
        found = resolve('/activity/add/')
        self.assertEqual(found.func, activity_add)
