from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.activity, name='activity'),  # Halaman list kegiatan
    # Halaman form kegiatan
    path('add/', views.activity_add, name='activity_add'),
]
