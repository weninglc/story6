from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Activity, Participant
from .forms import ActivityForm, ParticipantForm

# Create your views here.

# Nampilin list kegiatan yang ada + peserta yang terdaftar


def activity(request):
    submit = False
    activity_list = Activity.objects.all()
    participant_list = Participant.objects.all()
    if request.method == 'POST':
        form = ParticipantForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/activity/?submit=True')
    else:
        form = ParticipantForm()
        if 'submit' in request.GET:
            submit = True
    return render(request, 'activity_list.html', {'form': form, 'submit': submit, 'activity_list': activity_list, 'participant_list': participant_list})

# Nampilin form untuk nambah kegiatan


def activity_add(request):
    submit = False
    if request.method == 'POST':
        form = ActivityForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/activity/add/?submit=True')
    else:
        form = ActivityForm()
        if 'submit' in request.GET:
            submit = True
    return render(request, 'activity_add.html', {'form': form, 'submit': submit})
